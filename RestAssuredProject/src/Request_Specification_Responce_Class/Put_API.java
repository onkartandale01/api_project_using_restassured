package Request_Specification_Responce_Class;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Put_API {

	public static void main(String[] args) {
		// Step 1 : Collect all needed information and save it into local variables

		String req_body = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";

		String hostname = "https://reqres.in";

		String resource = "/api/users/2";

		String headername = "Content-Type";

		String headervalue = "application/json";

		// Step 2 : Build the request specification using RequestSpecification class

		RequestSpecification requestSpec = RestAssured.given();

		// Step 2.1 : Set request header

		requestSpec.header(headername, headervalue);

		// Step 2.2 : Set request body

		requestSpec.body(req_body);

		// Step 3 : Send the API request

		Response response = requestSpec.put(hostname + resource);
		System.out.println(response.getBody().asString());


	}

}
